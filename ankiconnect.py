import json
import urllib.request
import sys
import tempfile
import os

def request(action, **params):
    return {'action': action, 'params': params, 'version': 6}

def invoke(action, **params):
    requestJson = json.dumps(request(action, **params)).encode('utf-8')
    response = json.load(urllib.request.urlopen(urllib.request.Request('http://localhost:8765', requestJson)))
    if len(response) != 2:
        raise Exception('response has an unexpected number of fields')
    if 'error' not in response:
        raise Exception('response is missing required error field')
    if 'result' not in response:
        raise Exception('response is missing required result field')
    if response['error'] is not None:
        raise Exception(response['error'])
    return response['result']

def getNoteTypes():
    return invoke('modelNames')

def getFields(noteType):
    return invoke('modelFieldNames', modelName=noteType)

def getDecks():
    return invoke('deckNames')

def getCards(noteType):
    cards = invoke('modelTemplates', modelName=noteType)
    return [card for card in cards]

def getTemplate(noteType, card):
    cards = invoke('modelTemplates', modelName=noteType)
    style = invoke('modelStyling', modelName=noteType)
    return cards[card], style['css']

def getTags():
    return invoke('getTags')

def addNote(note):
    options = {
        'allowDuplicate': False,
        'duplicateScope': note['deckName']
    }
    note['options'] = options
    return invoke('addNote', note=note)

if __name__ == '__main__':
    #print(getNoteTypes())
    #print(getFields('Ultimate Geography'))
    #print(getDecks())
    print(getTemplate('Basic'))
    #print(getTags())
    #print(addNote('Basic', 'Test', {'Front': 'Hello World!', 'Back': 'From Python...'}))

QUIT             = 'q'  # (q)uit

SELECT_NOTE_TYPE = 'n'  # select (n)ote type
SELECT_DECK      = 'd'  # select (d)eck
SELECT_TAGS      = 'g'  # select ta(g)s
PIN_FIELD        = 'p'  # (p)in field

ENTER            = '\r' # (e)nter
PREVIEW          = 'f'  # preview (f)ront
PREVIEW_BACK     = 'b'  # preview (b)ack
ADD              = 'a'  # (a)dd card

PREV             = 'i'  # j
NEXT             = 't'  # k

ESCAPE           = '\x1b'
